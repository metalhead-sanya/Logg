//
//  Logg.swift
//  Logg
//
//  Created by Александр Залуцкий on 10/06/2018.
//  Copyright © 2018 Alexander Zalutskiy. All rights reserved.
//

import Foundation
import os

/// A custom log object that can be passed to logging functions in order to
/// send messages to the logging system.
public
struct Log {
	
	public
	struct Category: RawRepresentable {
		public
		typealias RawValue = String
		
		public
		let rawValue: RawValue
		
		public
		init(rawValue: RawValue) {
			self.rawValue = rawValue
		}
	}
	
	/// Logging levels supported by the system.
	public
	struct Level: RawRepresentable {
		public
		typealias RawValue = UInt8
		
		public
		let rawValue: RawValue
		
		private
		let _osLogType: Any?
		
		@available(iOS 10.0, *)
		fileprivate
		var osLogType: OSLogType {
			return _osLogType as! OSLogType
		}
		
		public init(rawValue: RawValue) {
			self.rawValue = rawValue
			
			if #available(iOS 10.0, *) {
				_osLogType = OSLogType(rawValue)
			} else {
				_osLogType = nil
			}
		}
		
		/// The default log level.
		public
		static var `default`: Level {
			if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
				return Level(rawValue: OSLogType.default.rawValue)
			} else {
				return Level(rawValue: 1)
			}
		}
		
		/// The info log level.
		public
		static var info: Level {
			if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
				return Level(rawValue: OSLogType.info.rawValue)
			} else {
				return Level(rawValue: 2)
			}
		}
		
		/// The debug log level.
		public
		static var debug: Level {
			if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
				return Level(rawValue: OSLogType.debug.rawValue)
			} else {
				return Level(rawValue: 3)
			}
		}
		
		/// The error log level.
		public
		static var error: Level {
			if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
				return Level(rawValue: OSLogType.error.rawValue)
			} else {
				return Level(rawValue: 4)
			}
		}
		
		/// The fault log level.
		public
		static var fault: Level {
			if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
				return Level(rawValue: OSLogType.fault.rawValue)
			} else {
				return Level(rawValue: 5)
			}
		}
	}
	
	private
	let subsystem: String
	
	fileprivate
	let category: Category
	
	private
	var _osLog: Any?
	
	@available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *)
	fileprivate
	var osLog: OSLog {
		return _osLog as! OSLog
	}
	
	/// Creates a custom log object, to be passed to logging functions for
	/// sending messages to the logging system.
	///
	/// - parameter subsystem: An identifier string, in reverse DNS notation,
	///   representing the subsystem that’s performing logging. For example,
	///   com.your_company.your_subsystem_name. The subsystem is used for
	///   categorization and filtering of related log messages, as well as for
	///   grouping related logging settings.
	/// - parameter category: A category within the specified subsystem. The
	///   category is used for categorization and filtering of related log
	///   messages, as well as for grouping related logging settings within the
	///   subsystem’s settings. A category’s logging settings override those of
	///   the parent subsystem.
	/// - returns: A custom log object, which can be passed to other logging
	///   functions to perform logging and to determine whether a specific level
	///   of logging is enabled. A value is always returned and should be
	///   released when no longer needed.
	public init(subsystem: String, category: Category) {
		self.subsystem = subsystem
		self.category = category
		
		if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
			_osLog = OSLog(subsystem: subsystem, category: category.rawValue)
		} else {
			_osLog = nil
		}
	}
	
	/// The shared default log.
	public
	static let `default`: Log = {
		var log: Log
		let category = Category(rawValue: "")
		if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
			let osLog = OSLog.default
			log = Log(subsystem: "", category: category)
			log._osLog = osLog
		} else {
			log = Log(subsystem: "default", category: category)
		}
		return log
	}()
	
	/// The shared disabled log.
	public
	static let disabled: Log = {
		var log: Log
		let category = Category(rawValue: "")
		if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
			let osLog = OSLog.disabled
			log = Log(subsystem: "", category: category)
			log._osLog = osLog
		} else {
			log = Log(subsystem: "default", category: category)
		}
		return log
	}()
	
	/// Returns a Boolean value indicating whether a specific type of logging,
	/// such as default, info, debug, error, or fault, is enabled for a
	/// specified log object.
	///
	/// - parameter log: The `.default` constant or a custom log object
	///   previously created by the os_log_create function.
	/// - parameter type: A log type constant, such as `.default`, `.info`,
	///   `.debug`, `.error`, or `.fault`, indicating the level of logging to
	///   to check.
	/// - returns: true if logging at the specified level is enabled,
	///   otherwise false.
	func isEnabled(type: Level) -> Bool {
		let isEnabled: Bool
		if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
			let osType = OSLogType(rawValue: type.rawValue)
			return osLog.isEnabled(type: osType)
		} else {
			isEnabled = true
		}
		return isEnabled
	}
}

/// Sends a message to the logging system, optionally specifying a custom log
/// object, log level, and any message format arguments.
///
/// - parameter message: A constant string or format string that produces a
///   human-readable log message.
/// - parameter log: A custom log object. If unspecified, the shared default
///   log is used.
/// - parameter level: The log level. If unspecified, the default log level is
///   used.
/// - parameter args: If message is a constant string, do not specify arguments.
///   If message is a format string, pass the expected number of arguments in
///   the order that they appear in the string.
public
func log(message: StaticString,
		 log: Log = .default,
		 level: Log.Level = .default,
		 _ args: CVarArg...) {
	if #available(iOS 10.0, macOS 10.12, watchOS 3.0, tvOS 10.0, *) {
		os_log(message, log: log.osLog, type: level.osLogType, args)
	} else {
		NSLog("[\(log.category)] \(message)", args)
	}
}
