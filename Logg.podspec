Pod::Spec.new do |s|
  s.name         = "Logg"
  s.version      = "1.0.0"
  s.summary      = "A OSLog wrapper for simple usage on os without OSLog."
  s.description  = <<-DESC
					You can use this wrapper for using OSLog on operation system without
					OSLog functionality. On old version OS Logg use NSLog for logging
                   DESC
				   
  s.homepage     = "https://gitlab.com/metalhead-sanya/Logg"
  
  s.license      = { :type => "BSD 3 Clause", :file => "LICENSE" }
  
  s.author             = { "Alexander Zalutskiy" => "metalhead.sanya@gmail.com" }
  # s.social_media_url   = "http://twitter.com/"

  s.ios.deployment_target = "8.0"
  s.osx.deployment_target = "10.9"
  s.watchos.deployment_target = "2.0"
  s.tvos.deployment_target = "9.0"

  s.source       = { :git => "https://gitlab.com/metalhead-sanya/Logg.git", :tag => "#{s.version}" }

  s.source_files  = "Sources/**/*.{swift}"

  s.swift_version = "4.0"
  s.requires_arc = true
end
